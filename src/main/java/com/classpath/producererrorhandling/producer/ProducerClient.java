package com.classpath.producererrorhandling.producer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Function;

@Configuration
public class ProducerClient {

    @Bean
    public Function<String, String> producer(){
        return (input) ->{
            if(input.equals("true")) {
                return input.toUpperCase();
            } else {
                throw new IllegalStateException("Invalid value");
            }
        };
    }

}