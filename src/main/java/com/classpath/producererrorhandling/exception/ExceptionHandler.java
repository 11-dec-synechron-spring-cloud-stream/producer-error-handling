package com.classpath.producererrorhandling.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.support.ErrorMessage;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ExceptionHandler {

    @ServiceActivator(inputChannel = "topic/out/messages.errors")
    public void handleException(ErrorMessage errorMessage){
      log.error("Inside the handleException method :: {}", errorMessage.getOriginalMessage());
      log.error("Inside the handleException method :: {}", errorMessage.getPayload());
    }
}